[TOC]

# Overview

AWS S3 has very limited support for applying lifecycle rules to objects.
**S3it** (S3-if-then) addresses some of these limitations by performing defined
actions on S3 objects that have been selected based on fine grained selection
criteria.

The native S3 [bucket lifecycle
rules](https://docs.aws.amazon.com/AmazonS3/latest/dev/object-lifecycle-mgmt.html)
can only perform:

*   Transition actions to move objects from one storage class to another.
*   Expiration actions to delete objects after a certain time.

Moreover, the selection process is crude. Objects can be filtered by key prefix
and object tags and that's all.

Let's say you want to move all JSON files in a certain area of an S3 bucket to
another archive bucket, partitioned by date of last modfiication.  Not a chance
with native S3 but **s3it** can do it.

How about finding all objects over 10MB that haven't been modified for 3 months
and moving them to an archive area with an infrequent access storage class.
S3 says *no*. **S3it** says *yes*.

Need to find all unencrypted files in a bucket, encrypt them with KMS if they
are less than 1 year old, otherwise delete them? Yup.

Want to find and delete those pesky objects that look like an S3 prefix but are
really objects with a zero length basename? You know you do.

Feeling nervous about losing data? Want to try a dry-run first? S3 lifecycle
rules won't help but **s3it** allows this.

**S3it** is based on *action* specifications that implement an *if* / *then*
model to provide fine grained S3 object selection and flexible manipulation of
selected objects.

The *if* part of an action selects S3 objects based on one
or more criteria such as:

*   time since last modification
*   object name pattern matching
*   tag values
*   size
*   object metadata
*   depth in the bucket
*   storage class.

The *then* part of an action specifies commands to perform
on the object such as:

*   printing selected information about the object
*   copying it elsewhere in S3
*   moving it elsewhere in S3
*   adding or modifying tags
*   changing storage class
*   modifying encryption settings
*   setting content type.

Command parameters can reference various metadata elements associated with the
object.

# Action Specifications

Action files are YAML files containing a list of action specifications.

The following example contains two actions.


```yaml
actions:

  # Action 1:
  #     List and remove remove all objects in `my-bucket-1` that end in
  #     .json and which have not been modified in the last 2 weeks.

  - bucket: my-bucket-1
    if:
      - glob *.json
      - older 2w
    then:
      - echo "About to remove {{key}}"
      - rm

  # Action 2:
  #     Find all objects under the "data" prefix in my-bucket-2 that are larger
  #     than 20 Mbytes and don't have a tag "Keep" with value "yes". The
  #     selected objects will be moved to the "Archive" area of the same bucket
  #     in a sub area with a name derived from the current date but retaining
  #     the original object basename.

  - bucket: my-bucket-2
    prefix: data
    if:
      - bigger 20MB
      - not tag Keep=yes
    then:
      - mv "s3://{{bkt}}/Archive/{{now.strftime('%Y-%m-%d')}}/"
```

The following keys are allowed in actions. Any other keys will be ignored.

|Key|Required|Type|Description|
|-|-|-|
|bucket|Yes|String|The bucket name.|
|id|No|String|An arbitrary identifier for the action. A command line option allows selective execution of actions based on identifiers.|
|if|No|List or string|One or more selection tests that will be applied to objects. See [Selecting Objects](#markdown-header-selecting-objects) for more information. If not specified, all objects are selected.|
|prefix|No|String|A prefix within the bucket to scan for objects. If not specified, the whole bucket is scanned.|
|then|No|List or string|One or more commands to perform for selected objects. See [Executing Commands](#markdown-header-executing-commands). If not specified, the selected object names are printed to stdout.|

# Selecting Objects

Each action specification may contain an `if` key consisting of a list of
selection tests that are applied to each candidate object as determined by the
`bucket` and `prefix` keys. The tests are performed in order and the object is
excluded from further processing once any test fails. Hence its best to put any
expensive tests later in the sequence.

All tests are of the form:

```yaml
if:
  - <TEST_TYPE> [ARGS...]
  - ...
```

or

```yaml
if:
  - not <TEST_TYPE> [ARGS...]
  - ...
```

Prefixing the test type with `not` as shown in the second case, inverts the
sense of the test.

The arguments are test type specific. The S3 object being tested is always an
implicit argument and does not need to be included unless otherwise indicated.


## Object Selection Tests


### bigger

Return true if the object is larger than a specified size.

Requires a single string argument of the form `nnnX` where `nnn` is an integer or float
and `X` is one of (case sensitive):

*   `B`:         Bytes
*   `K`, `KB`:  Kilobytes (1000)
*   `M`, `MB`:  Megabytes
*   `G`, `GB`:  Gigabytes
*   `T`, `TB`:  Terabytes
*   `P`, `PB`:  Petabytes.
*   `KiB`:      Kibibytes (1024)
*   `MiB`:      Mebibytes
*   `GiB`:      Gibibytes
*   `TiB`:      Tebibytes
*   `PiB`:      Pebibytes

Examples:

```yaml
# Select objects between 1 MB and 1GB in size
- bigger 1M
- not bigger 1GB
```

Whitespace is ignored.  Note a leading + or - will be handled correctly as will
exponentials. If no multiplier suffix is provided, bytes are assumed.


### class

Return true if the object has any of the specified [S3 storage
classes](https://docs.aws.amazon.com/AmazonS3/latest/dev/storage-class-intro.html).

Requires one or more storage class arguments. Currently accepted values are:

*   STANDARD
*   REDUCED_REDUNDANCY
*   STANDARD_IA
*   ONEZONE_IA
*   GLACIER

Examples:

```yaml
# Select objects in STANDARD or REDUCED_REDUNDANCY storage classes.
- class STANDARD REDUCED_REDUNDANCY

# Ignore objects in any of the infrequently accessed classes
- not class STANDARD_IA ONEZONE_IA
```

### depth

Return true if the object is at the specified depth or deeper in the bucket
with respect to the prefix specified in the action (i.e. not absolute depth).

Requires a single integer argument. Objects directly under the specified
prefix are at depth 1.

Examples:

```yaml
# Select only objects directly below the prefix
- not depth 2
```

### exec

Run a local command and return true if the command has a zero exit status.

The arguments are the command and its arguments. By default, a shell is
not invoked. If this is required then the shell must be explicitly invoked.
Arguments can be quoted as required.

Unlike most other `if` tests, the arguments for the `exec` test are
rendered to allow object specific details to be included. See
[Rendering of Arguments](#markdown-header-rendering-of-arguments).

The S3 object is not an implicit argument for this test. Use argument
rendering to do this if required.

>**Warning**: Do not use this on untrusted buckets.

Examples:

```yaml
# This will never return true
- exec /bin/sh -c "exit 1"

# Execute a test with bucket and object name as arguments.
- exec my-fancy-test --bucket {{bkt}} --key {{key}}

# Select objects that were modified on New Year's day.
- exec test {{obj.LastModified.month}} -eq 1 -a {{obj.LastModified.day}} -eq 1
```

### glob

Return true if the basename of the object matches any of the specified glob
patterns.

The arguments are one or more UNIX style glob patterns. It's best to enclose
them in quotes to avoid upsetting the YAML parser.

Examples:

```yaml
# Select compressed files provided they are not tar files
- glob "*.bz2" "*.zip" "*.gzip"
- not glob "*.tar.bz2" "*.tar.zip" "*.tar.gzip"
```

### meta

Return true if the object has the specified metadata.

Requires one or more arguments of the form `Name=Value` where `Name` is the
metadata field name.  If multiple arguments are specified, a match occurs if any
of the values match (i.e. this is an *OR* operation).

To achieve an *AND* criteria, use multiple `meta` conditions.


The available metadata fields are listed under
[S3 Object Metadata](#markdown-header-33-object-Metadata).


Examples:

```yaml
# Select objects that are not KMS encrypted
- not meta ServerSideEncryption=aws:kms

# Select all objects that are server-side encrypted.
- meta ServerSideEncryption=aws:kms ServerSideEncryption=AES256
```

This test requires an additional S3 API call per object, so it generally should
be late in the list of tests.

### older

Return true if the object has a last modification datetime greater than
the specified datetime.

Requires a single *duration* argument that species a time in the past relative
to the current time. The duration string is of the form `nnnX` where `nnn` is an
integer or float and `X` is one of (case sensitive):

* `w`:    weeks
* `d`:    days
* `h`:    hours
* `m`:    minutes
* `s`:    seconds.

If `X` is missing, seconds are assumed.

Examples:

```yaml
# Select objects that haven't been modified for a year.
- older 52w

# Select objects that have been modified in the last hour
- not older 1h
```

### prefix

Return true if the name of the object starts with any of the specified prefixes.
This is mostly useful with a `not` prefix to prevent action on specified areas
of the bucket as the `prefix` action key (or the equivalent `-p`, `--prefix`
command line option) is a far more efficient way to scope the activity to a
target prefix.

Examples:

```yaml
# Don't touch objects in the "precious/" or "leave/me/alone/" areas.
- not prefix precious/ leave/me/alone/
```

### tag

Return true if the object has tags with given values.

Requires one or more arguments in either of the these forms:

*   `Name=Value`
*   `Name`

The first form requires the specified tag have the given value. The second
form accepts any value provided the tag is present.

If multiple tags are specified a match occurs if any of the tags match (i.e.
this is an *OR* operation).

To achieve an *AND* criteria, use multiple `tag` conditions.

Examples:

```yaml
# Select objects that have the Keep or Precious tags set to yes
- tag Keep=yes Precious=yes

# Select objects that don't have a BillingCode tag
- not tag BillingCode
```

This test requires an additional S3 API call, so it should generally
be late in the list of tests.


# Executing Commands

An object that passes all of the `if` selection functions for the action
will have the commands specified in the `then` key performed on them.
The commands are performed in order.

All commands are of the form:

```yaml
then:
  - <COMMAND> [ARGS...]
  - ...
```

The command arguments are command specific and can contain references to
variables from the object and the processing context. The arguments are
[rendered](#markdown-header-rendering-of-arguments) before the command is
executed to inject variable values enclosed in `{{...}}`. The S3 object being
acted upon is always an implicit argument and does not need to be included
unless otherwise indicated.

## Commands

### acl

Apply the specified pre-canned ACL to the object.

Currently supported values are :

*   `private`
*   `public-read`
*   `public-read-write`
*   `aws-exec-read`
*   `authenticated-read`
*   `bucket-owner-read`
*   `bucket-owner-full-control`

Examples:

```yaml
# Make the object private
-- acl private
```

### class

Move the object to the specified S3 storage class but in the same location. This
is basically a convenience command that uses the [cp command](#markdown-header-cp)
to do the work.

Currently supported values are

*   `STANDARD`
*   `STANDARD_IA`
*   `ONEZONE_IA`
*   `REDUCED_REDUNDANCY`

Examples:

```yaml
# Move the object to reduced redundancy storage
- class REDUCED_REDUNDANCY
```

Note that `GLACIER` is not supported as the only way to move objects to glacier
is with a standard S3 lifecycle policy. However, you can work around this by
using **s3it** to add a distinctive tag to objects that you want moved to
glacier and use a lifecycle policy that filters on the tag value.

### cp

Copy the object to the specified location in S3 or the local filesystem.

Requires one or more arguments.

The first argument specifies the destination. Prefix the destination with
`s3://` to specify a destination in S3, otherwise the local filesystem is
assumed. If the destination is S3 and ends with `/`, the basename of the source
object is appended. Similarly, if the destination is a directory in the local
filesystem, the object basename is appended.

Subsequent arguments specify additional parameters for the copy operation
when the destination is in S3. These must be in the form `Name=Value`. The
following parameters are supported and have the same meaning as specified in the
[AWS CLI S3 cp command](https://docs.aws.amazon.com/cli/latest/reference/s3/cp.html).

*   `ACL`
*   `ContentDisposition`
*   `ContentEncoding`
*   `ContentLanguage`
*   `ContentType`
*   `GrantFullControl`
*   `GrantRead`
*   `GrantReadACP`
*   `GrantWriteACP`
*   `SSEKMSKeyId`
*   `ServerSideEncryption`
*   `StorageClass`

Examples:

```yaml
# Copy the object into the archive area in the same bucket,
# keeping the same basename.
- cp "s3://{{bkt}}/archive/"

# Copy the object into the archive area in the same bucket,
# but with an additional path component based on the date when
# the object was last modified.
- cp "s3://{{bkt}}/{{obj.LastModified.strftime('%Y/%m/%d')}}/"

# Copy the object into the archive area in the same bucket,
# keeping the same basename. Encrypt the new object with
# the specified KMS key.
- cp "s3://{{bkt}}/archive/" "ServerSideEncryption=aws:kms" "SSEKMSKeyId=alias/mykey"

# Copy the object to the /tmp directory on the local system.
- cp /tmp
```

### echo

Echo the specified arguments to stdout.

Examples:

```yaml
# Echo the object name and size
echo "{{bkt}}/{{key}}" "{{obj.Size}}"
```

### exec

Run a local command.

The arguments are the command and its arguments. By default, a shell is
not invoked. If this is required then the shell must be explicitly invoked.
Arguments can be quoted as required.

The S3 object is not an implicit argument for this command.

>**Warning**: Do not use this on untrusted buckets.

Examples:

```yaml
# Copy the object to /tmp and compress it.
- cp /tmp
- exec bzip2 "/tmp/{{key}}"
```

### ls

List the object name on stdout.

This is a pale imitation of **ls(1)**. With no arguments, just the name of the
object is printed. This is the default command if no other commands are
specified.

If a `-l` argument is provided, the last modification time (UTC) and approximate
file size are included.

Examples:

```yaml
# Just print the name of selected objects.
- ls

# ... with date and size
- ls -l
```

The output of `ls -l` looks like this:

```
2018-06-03 21:53:11      688 pfx/small
2018-06-03 21:53:09  678.0KB pfx/larger
```

### meta

Set the specified metadata elements on the object.

This is basically a convenience command that uses the [cp
command](#markdown-header-cp) to copy the object onto itself with new metadata.
See the [cp command](#markdown-header-cp) for useful metadata elements that can
be set.

Examples:

```yaml
# Force the object to be encrypted with AES256.
- meta ServerSideEncryption=AES256

# Force the object to be KMS encrypted with the default S3 KMS key.
- meta ServerSideEncryption=aws:kms SSEKMSKeyId=alias/aws/s3

# Force the object to be KMS encrypted with a custom KMS key.
- meta ServerSideEncryption=aws:kms SSEKMSKeyId=alias/my-key

# Set the content type for JSON files
- meta ContentType=application/json
```


### mv

Copy the object to the specified location in S3 or the local filesystem.

This is identical to [cp](#markdown-header-cp) followed by
[rm](#markdown-header-rm).

The arguments are as for [cp](#markdown-header-cp).

### rm

Remove the object.

Accepts no arguments.

Examples:

```yaml
# Remove the object
- rm
```

### tag

Set the specified tags on the object. Unlike the underlying [PUT Object
tagging](https://docs.aws.amazon.com/AmazonS3/latest/API/RESTObjectPUTtagging.html)
API call, this command updates the existing tags instead of replacing them.

Requires one or more arguments in either of these forms:

*   `Name=Value` or
*   `Name`

The first form sets the value of the named tag to the given value. The value can
be empty.

The second form removes the named tag.


Examples:

```yaml
# Add a tag specifying when the object was actioned.
- tag "ActionDate={{utc.isoformat()}}"

# Add a tag Important and remove the tag Unloved
- tag Important=yes Unloved
```


# Rendering of Arguments

Command arguments are rendered using [Jinja2](http://jinja.pocoo.org) before
invoking the command, allowing injection of variable data into the command
arguments.

The full power of Jinja2 is available, however the most useful feature is
the ability to replace `{{variable}}` with the value of the variable.

The following variables are made available to the renderer for each S3
object that has been selected by the `if` conditions.

|Name|Type|Description|
|-|-|-|
|base|String|Basename of the object.|
|bkt|String|Bucket name as specified in the action.|
|dir|String|The "directory" component of the object key.|
|key|String|S3 object key, including the prefix. This is also available as `obj.Key`.|
|meta|Dictionary|[S3 object metadata](#markdown-header-S3-object-metadata).|
|now|Datetime|Current local time. See [Datetime Variables](#markdown-header-datetime-variables) below.|
|obj|Dictionary|[S3 object information](#markdown-header-s3-object-information) returned when the bucket is scanned using [GET Bucket (List Objects) Version 2](https://docs.aws.amazon.com/AmazonS3/latest/API/v2-RESTBucketGET.html).|
|prefix|String|S3 object prefix as specified in the action.|
|tail|String|The object key relative to the prefix specified in the action. i.e. the part of the object key after the prefix.|
|tags|Dictionary|Tags associated with the object.|
|utc|Datetime|Current time in UTC. See [Datetime Variables](#markdown-header-datetime-variables) below.|

The various object key variables relate as follows:

*   `key` = `prefix` / `tail`
*   `key` = `dir` / `base`

For example, if the prefix specified in the action is `a/b` and the selected
object key is `a/b/c/d`, the object key related variables have these
values:

|Name|Value|
|-|-|
|base|d|
|dir|a/b/c|
|key|a/b/c/d|
|prefix|a/b|
|tail|c/d|

Normal [Jinja2](http://jinja.pocoo.org) features can be used to derive other
variants as required.

## S3 Object Information

The `obj` key made available to the command argument renderer contains the
following elements relating to the S3 object.

|Name|Type|Description|
|-|-|-|
|ETag|String|The entity tag is an MD5 hash of the object.|
|Key|String|Object key.|
|LastModified|Datetime|Date and time the object was last modified (UTC). See [Datetime Variables](#markdown-header-datetime-variables) below.|
|Owner|Dictionary|Bucket owner. Contains `DisplayName` and `ID` child strings.|
|Size|Integer|Object size in bytes.|
|StorageClass|String|S3 storage class. Currently supported values are `STANDARD`, `STANDARD_IA`, `ONEZONE_IA`, `REDUCED_REDUNDANCY` and `GLACIER`.|

These values can be referenced in command arguments using standard Jinja2
syntax. e.g.:

*   `{{obj.Size}}`
*   `{{obj.Owner.DisplayName}}`

## S3 Object Metadata

The `meta` key made available to the command argument renderer contains the
following S3 object metadata.

|Name|Type|Description|
|-|-|-|
|AcceptRanges|String|Not useful.|
|ContentType|String|MIME content-type of the object.|
|ContentLength|String|Use `obj.Size` instead.|
|ETag|String|Use `obj.ETag` instead.|
|LastModified|Datetime|Use `obj.LastModified` instead.|
|ResponseMetadata|Dictionary|Metadata relating to the API response. There is nothing useful in here that cannot be accessed more easily some other way.|
|SSEKMSKeyAlias|String|Alias for the SSM KMS key if the object is KMS encrypted and the alias can be determined. This is not actually part of the S3 metadata for the object but is provided as a convenience. Requires appropriate IAM permissions for KMS.|
|SSEKMSKeyId|String|SSM KMS key ARN if the object is KMS encrypted.|
|ServerSideEncryption|String|Encryption algorithm used if the object is encrypted. Typically `AES256` or `aws:kms`.|

These values can be referenced in command arguments using standard Jinja2 syntax. e.g.:

*   `{{meta.SSEKMSKeyId}}`

## Datetime Variables

A number of datetime variables are made available for injection into command
arguments. Most of the standard Python [datetime attributes and
methods](https://docs.python.org/3/library/datetime.html#datetime-objects)
are then available by virtue of Jinja2.


Useful attributes include:

*   **year**
*   **month**
*   **day**

Useful methods include:

*   **strftime**(*format*)
*   **timestamp**()
*   **isoformat**()

Of these,
[strftime](https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior)
is particularly useful as it allows arbitrary date structures based on the current time
or S3 object last modified time to be included in command arguments.

Usage follows standard Jinja2 syntax which is essentially the normal Python calling
convention. e.g.

*   `{{utc.isoformat()}}`
*   `{{now.year}}`
*   `{{obj.LastModified.strftime('%Y-%m-%d')}}`

# Installation and Usage

## Requisites

**S3it** works on Linux and Mac OSX. DOS is not currently supported.

It requires Python 3.3+ (3.6+ is recommended.) and does not use any exotic
non-standard modules.

Why no Python 2.7? Seriously?

## Installation


To install, clone the repo then:

```bash
make install

# ... or possibly
sudo make install
```

This will create an `s3it` directory in `/usr/local/lib` containing the code and
all the required Python support modules encapsulated by `virtualenv`. No Python
modules will be installed in your system environment.

An entry script that will activate the virtualenv when required is installed as
`/usr/local/bin/s3it` and a manual entry is installed in
`/usr/local/share/man/man1` where **man(1)** should find it automatically.

If you get errors trying to reinstall, simply remove the `/usr/local/lib/s3it`
directory and try again.

Alternatively, if you're happy to install Python modules globally you can
do:

```bash
# Clone the repo first then...
umask 022  # Just in case

# Install the required modules -- Its just boto3, pyyaml and jinja2
sudo python3 -m pip install -r requirements.txt --upgrade

# Install the s3it executable
install -m755 s3it.py /usr/local/bin/s3it

# Install the manual
install -m644 s3it.1 /usr/local/share/man/man1
```

## Usage

**S3it** can be used from the command line or scheduled via **cron(1)**. It can
read its action specification files from the local file system or S3.

Usage details can be found using:

```bash
s3it --help
```

For more information:

```bash
man s3it
```

The [samples directory](samples) contains some example action specification files.
