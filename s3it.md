# s3it(1) -- Rules based manipulation of AWS S3 objects

## SYNOPSIS

`s3it` \[options\] action-file ...

## DESCRIPTION

AWS S3 has very limited support for applying lifecycle rules to objects.
**S3it** (S3-if-then) addresses some of these limitations by performing defined
actions on S3 objects that have been selected based on fine grained selection
criteria.

## OPTIONS

### General Options

*   _action-file_:
    Action definition files. Prefix with s3:// for S3 based files. See [ACTION FILES]
    for more information.

*   `-a` _ACTION_, `--action` _ACTION_:
    Run only the action with the specified ID. Can be repeated to select
    multiple actions.

*   `-b` _BUCKET_, `--bucket` _BUCKET_:
    Bucket name. Used (and required) if the action specifications do not specify
    a target bucket. This option, together with `-p`, `--prefix` allows action
    specifications to be written independently of the specific target area
    in S3 and then targeted at run-time.

*   `-d`, `--dry-run`:
    Do a dry run. No modify actions are performed but actions that would be
    performed are identified.

*   `-h`, `--help`:
    Print help and exit.

*   `--lock-file` _LOCK_FILE_:
    Create a lock file with the given name to prevent two instances using the
    same lock file from running simultaneously. This is handy when running
    **s3it** via cron(1).

*   `-p` _PREFIX_, `--prefix` _PREFIX_:
    Prefix in the target bucket. Used if the action specifications do not
    specify a prefix. This option, together with `-b`, --bucket` allows action
    specifications to be written independently of the specific target area
    in S3 and then targeted at run-time
    

*   `--profile` _profile-id_:
    As for the AWS CLI.

*   `-v`, `--version`:
    Print version and exit.

### Logging Options

*   `-c`, `--no-colour`, `--no-color`:
    Don't use colour in information messages.
    
*   `-l` _LEVEL_, `--level` _LEVEL_:
    Print messages of a given severity level or above. The standard logging
    level names are available but <info>, <warning> and <error> are most useful.
    Default is <info>.

*   `--log` _LOG_:
    Log to the specified target. This can be either a file name or a syslog
    facility with an @ prefix (e.g.  <@local0>).

*  `--tag` _TAG_:
    Tag log entries with the specified value. The default is <s3it>.
    
## ACTION FILES

Action files are YAML files containing an `actions` key that is a list of action
specifications (actions).

The following keys are allowed in actions. Any other keys will be ignored.
All keys are optional.

*   `id`:
    An arbitrary identifier for the action. The `-a`, `--action` command line
    option allows selective execution of actions based on identifiers.

*   `bucket`:
    The bucket name.

*   `prefix`:
    A prefix within the bucket to scan for objects. If not specified, the whole
    bucket is scanned.

*   `if`:
    A list of selection tests that will be applied to objects. If not
    specified, all objects are selected.

*   `then`:
    A list of commands to perform for selected objects. If not specified, the
    selected object names are printed to stdout.


The following example contains two actions.


```yaml
actions:

  # Action 1:
  #     List and remove remove all objects in `my-bucket-1` that end in
  #     .json and which have not been modified in the last 2 weeks.

  - bucket: my-bucket-1
    if:
      - glob *.json
      - older 2w
    then:
      - echo "About to remove {{key}}"
      - rm

  # Action 2:
  #     Find all objects under the "data" prefix in my-bucket-2 that are larger
  #     than 20 Mbytes and don't have a tag "Keep" with value "yes". The
  #     selected objects will be moved to the "Archive" area of the same bucket
  #     in a sub area with a name derived from the current date but retaining
  #     the original object basename.

  - bucket: my-bucket-2
    prefix: data
    if:
      - bigger 20MB
      - not tag Keep=yes
    then:
      - mv "s3://{{bkt}}/Archive/{{now.strftime('%Y-%m-%d')}}/"
```
## SEE ALSO

A more comprehensive manual is available at
[https://bitbucket.org/murrayandrews/s3it](https://bitbucket.org/murrayandrews/s3it)

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).
