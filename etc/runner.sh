#!/bin/sh

# General runner for Python programs. If the basename of this script is xxx,
# then look for a runnable Python script in one of the following (with and
# without the .py extension):
# 	LIBDIR/xxx.d/xxx.py
#	LIBDIR/xxx/xxx.py
#	LIBDIR/venv*/bin/xxx.py
#	LIBDIR/xxx.py
#
# A typical usage would be to have this script reside in /usr/local/bin and
# there would be a corresponding directory in /usr/local/lib with the same name
# containing the main python script and whatever supporting modules or files it
# requires.
#
# If there is a virtual environment in the LIBDIR (named venv*),
# then activate that before running the script.
#
# This is all a bit clunky but it allows a Python based program with its own
# local modules and other module dependences to be installed as an isolated
# bundle that can be easily uninstalled.

PROG=`basename $0`
LIBDIR=/usr/local/lib

#-------------------------------------------------------------------------------
# Find the runnable script
for d in $LIBDIR/${PROG}.d $LIBDIR/$PROG $LIBDIR/$PROG/venv*/bin $LIBDIR
do
	for r in $d/$PROG.py $d/$PROG
	do
		if [ -f "$r" -a -x "$r" ] 
		then
			RUNNABLE="$r"
			LIBDIR="$d"
			break 2
		fi
	done
done

[ "$RUNNABLE" = "" ] && echo "$PROG: Cannot locate run module in $LIBDIR" >&2 && exit 1

#-------------------------------------------------------------------------------
# See if there is virtualenv for it - must be in same dir as main script
for v in $LIBDIR/venv*
do
	[ -d $v ] && VENV=$v && break
done

[ "$VENV" != "" ] && . $VENV/bin/activate

#-------------------------------------------------------------------------------

exec $RUNNABLE "$@"
