#!/bin/bash
# Install s3it

TARGET=s3it
MANFILES=s3it.1
python=python3  # Prefer python or python3.
force_python_version=yes  # Set to non-empty value to prevent choice of version

# ##############################################################################
# Import all the boilerplate which does a lot of the install
. $(dirname $0)/install-common.sh "$@"
# ##############################################################################

# ##############################################################################
# region Custom components

