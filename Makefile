INSTALLDIR=/usr/local

# ------------------------------------------------------------------------------

PROGRAMS = s3it

MD_TO_MAN = ronn --roff --organization MA --manual AWStools --date `date +'%Y-%m-%d'`
MAN_SECTION = 1
MANFILES=$(addsuffix .$(MAN_SECTION),$(PROGRAMS))

%.$(MAN_SECTION): %.md
	$(MD_TO_MAN) $<

.PHONY:	help gitclean man install force clean

help:	# Default target
	@echo
	@echo "What do you want to make?"
	@echo
	@echo "Available targets:"
	@echo
	@echo "    help:	This message"
	@echo "    install:	Install everything (git status must be clean)"
	@echo "    force:	Install everything (even if git status is not clean)"
	@echo "    bin:	Install programs only (even if git status is not clean)"
	@echo "    man:	Create manual entries locally"
	@echo "    clean:	Remove files that can be regenerated"
	@echo

gitclean:
	@if [ "`git status -s`" != "" ]; \
		then \
				echo "git status is not clean - cannot install"; \
				exit 1; \
		fi

man:	$(MANFILES)

install: gitclean force

force:	man
	etc/install.sh $(INSTALLDIR) < /dev/null

clean:
	$(RM) -r __pycache__
