#!/usr/bin/env python3

"""

Copyright (c) 2018, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

# ..............................................................................
# region imports

from __future__ import print_function

import subprocess
import argparse
from functools import lru_cache
import fcntl
import logging
import os
import re
import stat
import sys
from datetime import datetime, timedelta, timezone
from fnmatch import fnmatchcase
from logging.handlers import SysLogHandler
import shlex
import jinja2

import boto3
import yaml

# endregion imports
# ..............................................................................

# ..............................................................................
# region constants

__author__ = 'Murray Andrews'
__version__ = '1.2.0'

PROG = os.path.splitext(os.path.basename(sys.argv[0]))[0]
LOGNAME = PROG
LOGTARGET = None
LOGLEVEL = 'info'
LOG = logging.getLogger(name=LOGNAME)

ACTION_REQUIRED_KEYS = {'bucket'}
ACTION_OPTIONAL_KEYS = {'if', 'then', 'prefix'}

STRING_TYPES = (type('x'), type(u'x'))

# Params user can provide for an S3 copy operation
S3_COPY_PARAMS = {
    'ACL',
    'ContentDisposition',
    'ContentEncoding',
    'ContentLanguage',
    'ContentType',
    'GrantFullControl',
    'GrantRead',
    'GrantReadACP',
    'GrantWriteACP',
    'SSEKMSKeyId',
    'ServerSideEncryption',
    'StorageClass'
}

# endregion constants
# ..............................................................................

# ..............................................................................
# region colour

# ------------------------------------------------------------------------------
# Clunky support for colour output if colorama is not installed.

try:
    # noinspection PyUnresolvedReferences
    import colorama
    # noinspection PyUnresolvedReferences
    from colorama import Fore, Style

    colorama.init()

except ImportError:
    class Fore(object):
        """
        Basic alternative to colorama colours using ANSI sequences
        """
        RESET = '\033[0m'
        BLACK = '\033[30m'
        RED = '\033[31m'
        GREEN = '\033[32m'
        YELLOW = '\033[33m'
        BLUE = '\033[34m'
        MAGENTA = '\033[35m'
        CYAN = '\033[36m'


    class Style(object):
        """
        Basic alternative to colorama styles using ANSI sequences
        """
        RESET_ALL = '\033[0m'
        BRIGHT = '\033[1m'
        DIM = '\033[2m'
        NORMAL = '\033[22m'


# endregion colour
# ..............................................................................


# ..............................................................................
# region logging

# -------------------------------------------------------------------------------
def syslog_address():
    """
    Try to work out the syslog address.

    :return:    A value suitable for use as the address arg for SysLogHandler.
    :rtype:     tuple | str
    """

    for f in ('/dev/log', '/var/run/syslog'):
        try:
            mode = os.stat(f).st_mode
        except OSError:
            continue

        if stat.S_ISSOCK(mode):
            return f

    return 'localhost', 514


# -------------------------------------------------------------------------------
def get_log_level(s):
    """
    Convert the string version of a log level defined in the logging module to the
    corresponding log level. Raises ValueError if a bad string is provided.

    :param s:       A string version of a log level (e.g. 'error', 'info').
                    Case is not significant.
    :type s:        str

    :return:        The numeric logLevel equivalent.
    :rtype:         int

    :raises:        ValueError if the supplied string cannot be converted.
    """

    if not s or not isinstance(s, str):
        raise ValueError('Bad log level:' + str(s))

    t = s.upper()

    if not hasattr(logging, t):
        raise ValueError('Bad log level: ' + s)

    return getattr(logging, t)


# ------------------------------------------------------------------------------
class ColourLogHandler(logging.Handler):
    """
    Basic stream handler that writes to stderr but with different colours for
    different message levels.

    """

    # --------------------------------------------------------------------------
    def __init__(self, colour=True):
        """
        Allow colour to be enabled or disabled.

        :param colour:      If True colour is enabled for log messages.
                            Default True.
        :type colour:       bool

        """

        super(ColourLogHandler, self).__init__()
        self.colour = colour

    # --------------------------------------------------------------------------
    def emit(self, record):
        """
        Print the record to stderr with some colour enhancement.

        :param record:  Log record
        :type record:   logging.LogRecord
        :return:
        """

        if self.colour:
            if record.levelno >= logging.ERROR:
                colour = Style.BRIGHT + Fore.RED
            elif record.levelno >= logging.WARNING:
                colour = Fore.MAGENTA
            elif record.levelno >= logging.INFO:
                colour = Fore.BLUE
            else:
                colour = Style.DIM + Fore.BLACK

            print(colour + self.format(record) + Fore.RESET + Style.RESET_ALL, file=sys.stderr)
        else:
            print(self.format(record), file=sys.stderr)


# ------------------------------------------------------------------------------
def setup_logging(level, target=None, colour=True, name=None, prefix=None):
    """
    Setup logging.

    :param level:   Logging level. The string format of a level (eg 'debug').
    :param target:  Logging target. Either a file name or a syslog facility name
                    starting with @ or None. If None, log to stderr.
    :param colour:  If True and logging to the terminal, colourise messages for
                    different logging levels. Default True.
    :param name     The name of the logger to configure. If None, configure the
                    root logger.
    :param prefix:  Messages a prefixed by this string (with colon+space
                    appended). Default None.
    :type level:    str
    :type target    str | None
    :type colour:   bool
    :type prefix:   str | None
    :type name:     str | None

    :return:        The logger.
    :rtype:         logging.logger

    :raise ValueError: If an invalid log level or syslog facility is specified.
    """

    logger = logging.getLogger(name)
    logger.propagate = False
    logger.setLevel(get_log_level(level))

    # Get rid of unwanted handlers.
    for h in logger.handlers:
        logger.removeHandler(h)

    if target:
        if target.startswith('@'):
            # Syslog to specified facility

            if target[1:] not in SysLogHandler.facility_names:
                raise ValueError('Bad syslog facility: {}'.format(target[1:]))
            h = SysLogHandler(address=syslog_address(), facility=target[1:])
            h.setFormatter(
                logging.Formatter(
                    (prefix if prefix else '') + '[%(process)d]: %(levelname)s: %(message)s'
                )
            )
        else:
            # Log to a file
            h = logging.FileHandler(target)
            h.setFormatter(logging.Formatter('%(asctime)s: %(levelname)s: %(message)s'))
        logger.addHandler(h)
        logger.debug('%s', ' '.join(sys.argv))
        logger.debug('Logfile set to %s', target)
    else:
        # Just log to stderr.
        h = ColourLogHandler(colour=colour)
        h.setFormatter(logging.Formatter(
            (prefix + ': ' if prefix else '') + '%(message)s'))
        logger.addHandler(h)
    logger.debug('Log level set to %s (%d)', level, logger.getEffectiveLevel())

    return logger


# endregion logging
# ..............................................................................


# ..............................................................................
# region utils


# ------------------------------------------------------------------------------
def static_vars(**kwargs):
    """
    Allow a function to have static variables. Usage:

    @static_vars(v1=10, v2={}, ...)

    :param kwargs:      Variable names and initial values.
    :return:            Decorated function.
    """

    def decorate(func):
        """ Function to decorate. """
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func

    return decorate


# ------------------------------------------------------------------------------
TIME_UNITS = {
    'w': 60 * 60 * 24 * 7,
    'd': 60 * 60 * 24,
    'h': 60 * 60,
    'm': 60,
    's': 1,
    '': 1  # Default is seconds
}

DURATION_REGEX = r'\s*((?P<value>[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?)\s*(?P<units>[{units}]?))\s*$'.format(
    units=''.join(TIME_UNITS.keys())
)


def duration_to_seconds(duration):
    """
    Convert a string specifying a time duration to a number of seconds.

    :param duration:    String of the form nnnX where nnn is an integer or float
                        and X is one of (case sensitive):
                        'w':    weeks
                        'd':    days
                        'h':    hours
                        'm':    minutes
                        's':    seconds.

                        If X is missing then seconds are assumed. Whitespace is ignored.
                        Can also be a float or integer. Note a leading + or - will be
                        handled correctly as will exponentials.

    :type duration:     str | int | float
    :return:            The duration in seconds.
    :rtype:             float

    :raise ValueError:  If the duration is malformed.
    """

    if isinstance(duration, (int, float)):
        return float(duration)

    if not isinstance(duration, str):
        raise ValueError('Invalid duration type: {}'.format(type(duration)))

    m = re.match(DURATION_REGEX, duration)

    if not m:
        raise ValueError('Invalid duration: ' + duration)

    return float(m.group('value')) * TIME_UNITS[m.group('units')]


# ------------------------------------------------------------------------------
SIZE_UNITS = {
    'B': 1,
    'K': 1000,
    'KB': 1000,
    'M': 1000 ** 2,
    'MB': 1000 ** 2,
    'G': 1000 ** 3,
    'GB': 1000 ** 3,
    'T': 1000 ** 4,
    'TB': 1000 ** 4,
    'P': 1000 ** 5,
    'PB': 1000 ** 5,
    'KiB': 1024,
    'MiB': 1024 ** 2,
    'GiB': 1024 ** 3,
    'TiB': 1024 ** 4,
    'PiB': 1024 ** 5
}
SIZE_REGEX = r'\s*((?P<value>[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?)\s*(?P<units>{units}))\s*$'.format(
    units='|'.join(SIZE_UNITS.keys())
)


def size_to_bytes(size):
    """
    Convert a string specifying a data size to a number of bytes.

    :param size:        String of the form nnnX where nnn is an integer or float
                        and X is one of (case sensitive):
                        'B'         Bytes
                        'K', 'KB':  Kilobytes (1000)
                        'M', 'MB':  Megabytes
                        'G', 'GB':  Gigabytes
                        'T', 'TB':  Terabytes
                        'P', 'PB':  Petabytes.
                        'KiB':      Kibibytes (1024)
                        'MiB':      Mebibytes
                        'GiB':      Gibibytes
                        'TiB':      Tebibytes
                        'PiB':      Pebibytes

                        Whitespace is ignored.  Note a leading + or - will be
                        handled correctly as will exponentials. If no multiplier
                        suffix is provided, bytes are assumed.

    :type size:         str | int
    :return:            The size in bytes.
    :rtype:             int

    :raise ValueError:  If the input is malformed.
    """

    try:
        return int(size)
    except ValueError:
        pass

    if not isinstance(size, STRING_TYPES):
        raise ValueError('Invalid size type: {}'.format(type(size)))

    m = re.match(SIZE_REGEX, size)

    if not m:
        raise ValueError('Invalid size: ' + size)

    return int(round(float(m.group('value')) * SIZE_UNITS[m.group('units')], 0))


# ------------------------------------------------------------------------------
def num_to_scaled(n, unit='B', sep='', skip_unit=False):
    """
    Convert an unscaled number of some quantity to a human readable approximation
    using K (Kilo), M (Mega), G (Giga) ...

    Credit: https://stackoverflow.com/questions/1094841/reusable-library-to-get-human-readable-version-of-file-size

    :param n:           The number to convert.
    :param unit:        The unit of measure as a single char. The default is B
                        for bytes.
    :param sep:         Separator between the number and the unit. The default
                        is ''.
    :param skip_unit:   Don't include the unit for numbers that don't need a
                        scale factor. Default False.

    :type n:            int | float
    :type unit:         str
    :type sep:          str
    :type skip_unit:    bool

    :return:            A string representing the approximate number in scaled
                        human readable format.
    :rtype:             str
    """

    if n < 1000:
        return '{}{}{}'.format(n, '' if skip_unit else sep, '' if skip_unit else unit)

    for scale in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
        if abs(n) < 1000.0:
            return '{:.1f}{}{}{}'.format(n, sep, scale, unit)
        n /= 1000.0
    return '{:.1f}{}{}{}'.format(n, sep, 'Y', unit)


# ------------------------------------------------------------------------------
_lock_fp = None


def lock_file(fname):
    """
    Create a lock file with the given name and write the current process PID in
    it.

    :param fname:       Name of lockfile
    :type fname:        str

    :return:            True if lockfile created, False otherwise.

    :raise Exception:   If the lock file cannot be created.
    """

    global _lock_fp  # Has to stay in scope to stay open

    # Open as append so we don't obliterate PID of another process.
    _lock_fp = open(fname, 'a')  # Exceptions can propagate

    try:
        fcntl.lockf(_lock_fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError:
        return False

    # Now we have the lock - get rid of old PID contents.
    _lock_fp.truncate(0)

    # noinspection PyTypeChecker
    print(os.getpid(), file=_lock_fp)
    _lock_fp.flush()
    return True


# endregion utils
# ..............................................................................

# ..............................................................................
# region AWS utils

# ------------------------------------------------------------------------------
@lru_cache(maxsize=200)
def s3_get_object_metadata(bucket, key, s3_client=None):
    """
    Get the object metadata for the specified S3 object. Results are cached for
    a while.

    :param bucket:      Bucket name.
    :param key:         Object key.
    :param s3_client:   boto3 s3_client

    :type bucket:       str
    :type key:          str
    :type s3_client:    botocore.client.BaseClient

    :return:            The object info
    :rtype:             dict[str, T]
    """

    if not s3_client:
        s3_client = boto3.client('s3')

    return s3_client.head_object(Bucket=bucket, Key=key)


# ------------------------------------------------------------------------------
@lru_cache(maxsize=200)
def s3_get_object_tags(bucket, key, s3_client=None):
    """
    Get the object tags for the specified S3 object. Results are cached for a
    while.

    :param bucket:      Bucket name.
    :param key:         Object key.
    :param s3_client:   boto3 s3_client

    :type bucket:       str
    :type key:          str
    :type s3_client:    botocore.client.BaseClient

    :return:            A dictionary of tags
    :rtype:             dict[str, str]
    """

    if not s3_client:
        s3_client = boto3.client('s3')

    tags = s3_client.get_object_tagging(Bucket=bucket, Key=key)

    return {t['Key']: t['Value'] for t in tags.get('TagSet', [])}


# ------------------------------------------------------------------------------
@static_vars(kms_keys=None)
def kms_get_alias(key, aws_session=None):
    """
    Try to get the KMS key alias for a specified key. Results are cached.

    :param key:             KMS key ARN or key ID
    :param aws_session:     A boto3 Session(). If not specified, a default will
                            be created,

    :type key:              str
    :type aws_session:      boto3.Session

    :return:                The key alias or None if it cannot be found.
    :rtype:                 str

    """

    key_id = key.rsplit('/', 1)[-1]

    if kms_get_alias.kms_keys is None:
        kms_get_alias.kms_keys = {}

        if not aws_session:
            aws_session = boto3.Session()

        kms = aws_session.client('kms')

        for response in kms.get_paginator('list_aliases').paginate():
            for k in response.get('Aliases', []):
                try:
                    kms_get_alias.kms_keys[k['TargetKeyId']] = k['AliasName']
                except KeyError:
                    # Not all aliases have an associated key.
                    pass

    return kms_get_alias.kms_keys.get(key_id)


# ------------------------------------------------------------------------------
def s3_list_bucket(bucket, prefix=None, fetch_owner=True, s3_client=None):
    """
    List the contents of the S3 bucket under the specified prefix.

    :param bucket:      Bucket name.
    :param prefix:      Prefix. Default None.
    :param fetch_owner: If True, fetch owner information on objects.
                        Default True.
    :param s3_client:   Boto s3 client. If None, a default client is used.
                        Default None.

    :type bucket:       str
    :type prefix:       str
    :type fetch_owner:  bool
    :type s3_client:    botocore.client.BaseClient
    :return:            A generator yielding one S3 object at a time. The
                        yield value is a dict like this:
                        {
                            'Key': 'string',
                            'LastModified': datetime(2015, 1, 1),
                            'ETag': 'string',
                            'Size': 123,
                            'StorageClass': 'STANDARD'|'REDUCED_REDUNDANCY'|'GLACIER'|'STANDARD_IA'|'ONEZONE_IA',
                            'Owner': {
                                'DisplayName': 'string',
                                'ID': 'string'
                            }
                        }

    """

    if not s3_client:
        s3_client = boto3.client('s3')

    list_args = {
        'Bucket': bucket,
        'FetchOwner': fetch_owner,
    }
    if prefix:
        list_args['Prefix'] = prefix

    while True:
        response = s3_client.list_objects_v2(**list_args)
        for obj in response.get('Contents', []):
            yield obj

        try:
            list_args['ContinuationToken'] = response['NextContinuationToken']
        except KeyError:
            break


# ------------------------------------------------------------------------------
def s3_load_yaml(bucket_name, key, aws_session=None):
    """
    Load a YAML file from S3.

    :param bucket_name: Name of the S3 bucket.
    :param key:         Object key (file name) for the YAML file
    :param aws_session: A boto3 Session. If None a default is created.
    :type bucket_name:  str
    :type key:          str
    :return:            The object decoded from the YAML file.
    :rtype:             T

    :raise IOError:     If file doesn't exist or can't be retrieved.
    :raise ValueError:  If the file was retrieved but is not valid YAML.
    """

    if not aws_session:
        aws_session = boto3.Session()

    try:
        response = aws_session.resource('s3').Object(bucket_name, key).get()
    except Exception as e:
        raise IOError(str(e))

    try:
        return yaml.safe_load(response['Body'])
    except Exception as e:
        raise ValueError(str(e))


# ------------------------------------------------------------------------------
def s3_split(s):
    """
    Split an S3 object name into bucket and prefix components.

    :param s:       The object name. Typically bucket/prefix but the following
                    are also accepted:
                        s3:bucket/prefix
                        s3://bucket/prefix
                        /bucket/prefix

    :type s:        str
    :return:        A tuple (bucket, prefix)
    :rtype:         tuple(str, str)
    """

    # Clean off any s3:// type prefix
    for p in 's3://', 's3:':
        if s.startswith(p):
            s = s[len(p):]
            break

    t = s.strip('/').split('/', 1)

    if not t[0]:
        raise ValueError('Invalid S3 object name: {}'.format(s))
    return t[0], t[1].strip('/') if len(t) > 1 else ''


# endregion AWS utils
# ..............................................................................


# ------------------------------------------------------------------------------
class ActionError(Exception):
    """
    Generic action error.
    """

    pass


# ------------------------------------------------------------------------------
class Action(object):
    """
    Represents an S3 action directive.
    """

    action_num = 1

    # --------------------------------------------------------------------------
    # noinspection PyShadowingBuiltins
    def __init__(self, bucket, *, id=None, prefix=None, if_list=None,
                 then_list=None, aws_session=None):
        """
        Create an Action.

        :param bucket:          Bucket name.
        :param id:              Action id.
        :param prefix:          Prefix. Default None.
        :param if_list:         List of "if" selectors for picking objects in
                                the bucket. If None then everything will be
                                selected. Can be a string or list of strings.
        :param then_list:       List of "then" actions to perform on selected
                                bojects in the bucket. If None a default "echo"
                                action is performed. Can be a string or list
                                of strings.
        :param aws_session:     A boto3 Session object. If not supplied a
                                default session will be used.

        :type bucket:           str
        :type id:               str
        :type prefix:           str
        :type if_list:          list | str
        :type then_list:        list | str
        :type aws_session:      boto3.Session

        :raise ValueError:      If there are malformed or spurious arguments.

        """

        if os.path.sep != '/':
            raise NotImplementedError('DOS is not supported sorry')

        if not bucket:
            raise ValueError('bucket must be specified')

        self.aws_session = aws_session if aws_session else boto3.Session()
        self.s3 = self.aws_session.client('s3')

        self._render_args = True

        self.bucket = bucket
        self.prefix = prefix.strip('/') if prefix else None
        self.prefix_len = len(self.prefix.split('/')) if self.prefix else 0
        self.action_num = self.__class__.action_num
        self.__class__.action_num += 1
        self.id = id if id else str(self.action_num)

        self.if_list = [if_list] if isinstance(if_list, str) else if_list
        if not self.if_list:
            self.if_list = []
        if not isinstance(self.if_list, list):
            raise ValueError('"if" must be a list or string')

        self.then_list = [then_list] if isinstance(then_list, str) else then_list
        if not self.then_list:
            self.then_list = ['ls']
            self._render_args = False  # No point wasting time
        if not isinstance(self.then_list, list):
            raise ValueError('"then" must be a list or string')

    # --------------------------------------------------------------------------
    def _check_if(self, obj):
        """
        See if the given S3 object passes the conditions in the "if" clause of
        the action.

        :param obj:             S3 object dict (see s3_list_bucket())

        :type obj:              dict[str, T]

        :return:                True if the object passes all conditions.
        :rtype:                 bool
        """

        for if_item in self.if_list:  # type: str

            if_type, *args = shlex.split(if_item)
            if if_type == 'not' and args:
                required_outcome = False
                if_type = args[0]
                args = args[1:]
            else:
                required_outcome = True

            try:
                if getattr(self, 'if_' + if_type)(obj, *args) != required_outcome:
                    LOG.debug('Object s3://%s/%s failed if condition "%s %s"',
                              self.bucket, obj['Key'], if_type, ' '.join(args))
                    return False
            except AttributeError:
                raise ActionError('Unknown if condition type "{}"'.format(if_type))

        LOG.debug('Object s3://%s/%s is selected', self.bucket, obj['Key'])

        return True

    # --------------------------------------------------------------------------
    def do_actions(self, *, dry_run=False):
        """
        Perform the "then" actions on the specified bucket/prefix

        :param dry_run:         If True, report what would be done without doing
                                it. Default False.

        :type dry_run:          bool

        """

        if dry_run:
            LOG.info('Action: %s', self.id)
            LOG.info('Bucket: %s', self.bucket)

        # Scan the bucket for candidate objects
        for s3_obj in s3_list_bucket(self.bucket, self.prefix, s3_client=self.s3):
            LOG.debug('Action %s: Examining s3://%s/%s', self.id, self.bucket, s3_obj['Key'])

            if not self._check_if(s3_obj):
                continue

            # We have a candidate object -- apply the actions
            for t_idx, t in enumerate(self.then_list, start=1):
                LOG.debug('Action %s: processing then #%d - %s', self.id, t_idx, t)
                if not isinstance(t, str):
                    raise ActionError('"then" #{} - not a string'.format(t_idx))

                command, *args = shlex.split(t)

                if self._render_args:
                    # Get object metadata and dummy up an entry for the KMS key alias
                    # as a convenience.
                    metadata = s3_get_object_metadata(self.bucket, s3_obj['Key'], self.s3)

                    # noinspection PyBroadException
                    try:
                        metadata['SSEKMSKeyAlias'] = kms_get_alias(
                            key=metadata.get('SSEKMSKeyId'),
                            aws_session=self.aws_session
                        )
                    except Exception as e:
                        # Don't try too hard.
                        LOG.debug('Cannot lookup KMS key alias - %s', e)

                    # Render the args
                    dirname, base = os.path.split(s3_obj['Key'])
                    args = [
                        jinja2.Template(arg).render(
                            bkt=self.bucket,
                            pfx=self.prefix,
                            key=s3_obj['Key'],  # Just a convenience
                            dir=dirname,
                            base=base,
                            tail=os.path.relpath(s3_obj['Key'], self.prefix),
                            obj=s3_obj,
                            meta=metadata,
                            tags=s3_get_object_tags(self.bucket, s3_obj['Key'], self.s3),
                            now=datetime.now(),
                            utc=datetime.utcnow()
                        )
                        for arg in args
                    ]
                try:
                    then_cmd = getattr(self, 'then_' + command)
                except AttributeError:
                    raise ActionError('Unknown "then" command "{}"'.format(command))

                if dry_run:
                    LOG.info('        %s --> %s %s', s3_obj['Key'], command, ' '.join(['"' + a + '"' for a in args]))
                else:
                    then_cmd(s3_obj, args)

    # ..........................................................................
    # region if conditions

    # --------------------------------------------------------------------------
    @staticmethod
    def if_prefix(obj, *args):
        """
        Test whether the object key has any of the specified prefixes. This is
        mostly useful with a "not" prefix to prevent action on specified areas
        of the bucket.

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        Object prefixes

        :type obj:          dict[str, T]
        :type args:         str

        :return:            True if the object passes the test. False otherwise.
        :rtype:             bool

        """

        if not args:
            raise ValueError('At least one pattern required for "prefix" condition')

        key = obj['Key']  # type: str
        for prefix in args:
            if key.startswith(prefix):
                return True

        return False

    # --------------------------------------------------------------------------
    @staticmethod
    def if_glob(obj, *args):
        """
        Test whether the basename part of the key matches any of the given glob
        pattern.

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        glob patterns

        :type obj:          dict[str, T]
        :type args:         str

        :return:            True if the object passes the test. False otherwise.
        :rtype:             bool

        """

        if not args:
            raise ValueError('At least one pattern required for "glob" condition')

        obj_base = os.path.basename(obj['Key'])
        for pat in args:
            if fnmatchcase(obj_base, pat):
                return True

        return False

    # --------------------------------------------------------------------------
    @staticmethod
    def if_older(obj, *args):
        """
        Check if the last modified date is before a certain relative date (i.e
        x days/minutes/weeks ago).

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        A single duration string. See duration_to_seconds().

        :type obj:          dict[str, T]
        :type args:         str

        :return:            True if the object passes the test. False otherwise.
        :rtype:             bool

        """

        if len(args) != 1:
            raise ValueError('Exactly one duration string required for "older" condition')

        age = duration_to_seconds(args[0])
        now = datetime.utcnow().replace(tzinfo=timezone.utc)
        return obj['LastModified'] < now - timedelta(seconds=age)

    # --------------------------------------------------------------------------
    @staticmethod
    def if_bigger(obj, *args):
        """
        Check if the S3 object is bigger than a given size.

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        A single size string. See size_to_bytes().

        :type obj:          dict[str, T]
        :type args:         str

        :return:            True if the object passes the test. False otherwise.
        :rtype:             bool
        """

        if len(args) != 1:
            raise ValueError('Exactly one duration string required for "bigger" condition')

        return obj['Size'] > size_to_bytes(args[0])

    # --------------------------------------------------------------------------
    @staticmethod
    def if_class(obj, *args):
        """
        Check if the S3 object has any of the given storage classes.

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        One or more S3 storage class types (case
                            insensitive). The currently available AWS values
                            are:
                                - STANDARD
                                - REDUCED_REDUNDANCY
                                - STANDARD_IA
                                - ONEZONE_IA
                                - GLACIER

        :type obj:          dict[str, T]
        :type args:         str

        :return:            True if the object passes the test. False otherwise.
        :rtype:             bool
        """

        if not args:
            raise ValueError('At least one storage class type required for "class" condition')

        for c in args:
            if c.upper() == obj['StorageClass']:
                return True

        return False

    # --------------------------------------------------------------------------
    def if_depth(self, obj, *args):
        """
        Check if the S3 object is at the specified depth or deeper in the bucket
        with respect to the prefix (i.e. not absolute depth).

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        One or more integers representing relative depth
                            in the bucket.

        :type obj:          dict[str, T]
        :type args:         str

        :return:            True if the object passes the test. False otherwise.
        :rtype:             bool
        """

        if len(args) != 1:
            raise ValueError('Exactly one depth required for "depth" condition')

        obj_depth = len(obj['Key'].split('/')) - self.prefix_len
        return obj_depth >= int(args[0])

    # --------------------------------------------------------------------------
    def if_meta(self, obj, *args):
        """
        Check if the S3 object has metadata matching any of the given values.
        The comparison process is crude -- everything is converted to string.

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        Two arguments:
                                - Field name
                                - Field vale

        :type obj:          dict[str, T]
        :type args:         str

        :return:            True if the object passes the test. False otherwise.
        :rtype:             bool
        """

        if not args:
            raise ValueError('At least one argument required for "meta" condition')

        metadata = s3_get_object_metadata(self.bucket, obj['Key'], self.s3)

        # noinspection PyBroadException
        try:
            metadata['SSEKMSKeyAlias'] = kms_get_alias(
                key=metadata.get('SSEKMSKeyId'),
                aws_session=self.aws_session
            )
        except Exception as e:
            # Don't try too hard.
            LOG.debug('Cannot lookup KMS key alias - %s', e)

        for m in args:
            try:
                name, value = m.split('=', 1)
            except ValueError:
                raise ValueError('Malformed metadata parameter - {}'.format(m))

            if str(metadata.get(name, '')) == value:
                return True

        return False

    # --------------------------------------------------------------------------
    def if_tag(self, obj, *args):
        """
        Check if the S3 object has tags with given values

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        A series of tags in the form Name=Value. If multiple
                            tags are specified a match occurs if any of the tags
                            match (i.e. this is an OR operation).  If the tag
                            value is not specified then any value is accepted
                            provided the tag exists.

        :type obj:          dict[str, T]
        :type args:         str

        :return:            True if the object passes the test. False otherwise.
        :rtype:             bool
            """

        if not args:
            raise ValueError('At least one argument required for "tag" condition')

        tags = s3_get_object_tags(self.bucket, obj['Key'], self.s3)

        for t in args:
            try:
                name, value = t.split('=', 1)
                if tags.get(name) == value:
                    return True
            except ValueError:
                # No tag value specified -- any value will do
                if t in tags:
                    return True

        return False

    # --------------------------------------------------------------------------
    def if_exec(self, obj, *args):
        """
        Run a command to check if the object is selected. The command parameters
        are rendered with useful stuff.

        This does not use a shell by default. If that is required then the
        command should be "/bin/sh -c ...".

        DO NOT USE with untrusted input (i.e. on an untrusted bucket).

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        A shell command and its arguments.

        :type obj:          dict[str, T]
        :type args:         str

        :return:            True if the object passes the test. False otherwise.
        :rtype:             bool
        """

        if not args:
            raise ValueError('One or more arguments required for "exec" condition')

        # Get object metadata and dummy up an entry for the KMS key alias
        # as a convenience.
        metadata = s3_get_object_metadata(self.bucket, obj['Key'], self.s3)

        # noinspection PyBroadException
        try:
            metadata['SSEKMSKeyAlias'] = kms_get_alias(
                key=metadata.get('SSEKMSKeyId'),
                aws_session=self.aws_session
            )
        except Exception as e:
            # Don't try too hard.
            LOG.debug('Cannot lookup KMS key alias - %s', e)

        # Render the args -- we do it here rather than in the caller as
        # rendering is expensive and most "if" conditions don't need it.
        dirname, base = os.path.split(obj['Key'])
        args = [
            jinja2.Template(arg).render(
                bkt=self.bucket,
                pfx=self.prefix,
                key=obj['Key'],  # Just a convenience
                dir=dirname,
                base=base,
                tail=os.path.relpath(obj['Key'], self.prefix),
                obj=obj,
                meta=metadata,
                tags=s3_get_object_tags(self.bucket, obj['Key'], self.s3),
                now=datetime.now(),
                utc=datetime.utcnow()
            )
            for arg in args
        ]

        try:
            subprocess.check_call(
                args,
                stdin=subprocess.DEVNULL,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
            return True
        except subprocess.CalledProcessError as e:
            LOG.debug('if_exec %s returned status %d', args, e.returncode)
            return False

    # endregion if conditions
    # ..........................................................................

    # ..........................................................................
    # region then commands

    # --------------------------------------------------------------------------
    # noinspection PyUnusedLocal
    @staticmethod
    def then_echo(obj, args):
        """
        Echo the args.

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        A list of things to echo.
        :type obj:          dict[str, T]
        :type args:         list[str]
        """

        print(*args)

    # --------------------------------------------------------------------------
    def then_cp(self, obj, args, silent=False):
        """
        Copy the object to another S3 location or local file system.

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        These are the args:
                                - Target location. If it starts with s3:// then
                                  its elsehwere in S3, otherwise local file
                                  system.
                                - param=value...
                                  Optional copy parameters when the target is
                                  S3.
        :param silent:      If True, keep quiet. Default False.
        :type obj:          dict[str, T]
        :type args:         list[str]
        :type silent:       bool
        """

        if not args:
            raise ValueError('At least 1 argument required for cp command')

        if args[0].endswith('/'):
            args[0] += os.path.basename(obj['Key'])

        if args[0].startswith('s3://'):
            # S3 copy

            bucket, key = s3_split(args[0])

            copy_params = {
                'CopySource': {'Bucket': self.bucket, 'Key': obj['Key']},
                'Bucket': bucket,
                'Key': key,
                'MetadataDirective': 'REPLACE',
                'TaggingDirective': 'COPY',
            }

            for p in args[1:]:
                try:
                    k, v = p.split('=', 1)
                except ValueError:
                    raise ValueError('Malformed cp parameter - {}'.format(p))

                if k not in S3_COPY_PARAMS:
                    raise ValueError('Invalid cp parameter - {}'.format(k))
                copy_params[k] = v

            try:
                self.s3.copy_object(**copy_params)
                if not silent:
                    LOG.info('Copied s3://%s/%s to s3://%s/%s',
                             self.bucket, obj['Key'], bucket, key)
            except Exception as e:
                raise ActionError('Copy of s3://{}/{} to s3://{}/{} failed - {}'.format(
                    self.bucket, obj['Key'], bucket, key, e))
        else:
            # Copy to local file system
            if len(args) > 1 and not silent:
                LOG.warning('Extra arguments are ignored for copy to local file system')

            local_file = os.path.expanduser(args[0])

            if os.path.isdir(local_file):
                local_file = os.path.join(local_file, os.path.basename(obj['Key']))

            local_file = os.path.normpath(local_file)

            try:
                self.s3.download_file(
                    Bucket=self.bucket,
                    Key=obj['Key'],
                    Filename=local_file
                )
                if not silent:
                    LOG.info('Copied s3://%s/%s to %s', self.bucket, obj['Key'], local_file)
            except Exception as e:
                raise ActionError('Copy of s3://{}/{} to {} failed - {}'.format(
                    self.bucket, obj['Key'], local_file, e))

    # --------------------------------------------------------------------------
    def then_rm(self, obj, args, silent=False):
        """
        Remove the object

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        These are the args:
        :param silent:      If True, keep quiet. Default False.

        :type obj:          dict[str, T]
        :type args:         list[str]
        :type silent:       bool
        """

        if args:
            raise ValueError('The rm command does not accept arguments')

        try:
            self.s3.delete_object(
                Bucket=self.bucket,
                Key=obj['Key']
            )
            if not silent:
                LOG.info('Deleted s3://%s/%s', self.bucket, obj['Key'])
        except Exception as e:
            raise ActionError('Delete of s3://{}/{} failed - {}'.format(
                self.bucket, obj['Key'], e))

    # --------------------------------------------------------------------------
    def then_mv(self, obj, args):
        """
        Move the object to another S3 location. Basically a convenience
        operation which is a combination of cp + rm.


        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        As for then_cp()

        :type obj:          dict[str, T]
        :type args:         list[str]
        """

        self.then_cp(obj, args, silent=True)
        self.then_rm(obj, [], silent=True)
        LOG.info('Moved s3://%s/%s to %s', self.bucket, obj['Key'], args[0])

    # --------------------------------------------------------------------------
    @staticmethod
    def then_ls(obj, args):
        """
        List the object name.

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        A single optional argument "-l" which will cause
                            size and date information to be included a bit like
                            ls(1).

        :type obj:          dict[str, T]
        :type args:         list[str]
        """

        if not args:
            print(obj['Key'])
            return

        if len(args) == 1 and args[0] == '-l':
            print('{date} {size:>8} {key}'.format(
                date=obj['LastModified'].strftime('%Y-%m-%d %H:%M:%S'),
                size=num_to_scaled(obj['Size'], skip_unit=True),
                key=obj['Key']
            ))
        else:
            raise ValueError('Usage for "ls" condition is "ls [-l]')

    # --------------------------------------------------------------------------
    def then_class(self, obj, args):
        """
        Move the object to the specified storage class but in the same location.
        Basically a convenience operation which uses the then_mv() method.

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        The new storage class.

        :type obj:          dict[str, T]
        :type args:         list[str]
        """

        if len(args) != 1:
            raise ValueError('Exactly one argument required for "class" command')

        self.then_meta(obj, ['StorageClass=' + args[0]], silent=True)
        LOG.info('Set storage class for s3://%s/%s to %s', self.bucket, obj['Key'], args[0])

    # --------------------------------------------------------------------------
    def then_tag(self, obj, args):
        """
        Add tags to the object.

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        A series of tags in the form Name=Value

        :type obj:          dict[str, T]
        :type args:         list[str]
        """

        if not args:
            raise ValueError('One or more arguments required for "tag" command')

        # Get existing tags
        tags = s3_get_object_tags(self.bucket, obj['Key'], self.s3)

        # Merge in the new tags
        for t in args:
            try:
                k, v = t.split('=', 1)
            except ValueError:
                k, v = t, None

            tags[k] = v

        # Convert to the list of dicts format needed by AWS.
        tag_set = [{'Key': k, 'Value': v} for k, v in tags.items() if v is not None]

        try:
            self.s3.put_object_tagging(
                Bucket=self.bucket,
                Key=obj['Key'],
                Tagging={'TagSet': tag_set}
            )
            LOG.info('Tagged s3://%s/%s', self.bucket, obj['Key'])
        except Exception as e:
            raise ActionError('Tagging of s3://{}/{} failed - {}'.format(
                self.bucket, obj['Key'], e))

    # --------------------------------------------------------------------------
    def then_meta(self, obj, args, silent=False):
        """
        Set meta data for the object. This is very limited as a lot of metadata
        elements are read only. Useful for changing encryption settings and
        content type though.

        This is basically a convenience operation as cp does the work.

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        A series of metadata in the form Name=Value
        :param silent:      If True, keep quiet. Default False.

        :type obj:          dict[str, T]
        :type args:         list[str]
        :type silent:       bool
        """

        self.then_cp(
            obj,
            ['s3://{}/{}'.format(self.bucket, obj['Key'])] + args,
            silent=True
        )

        if not silent:
            LOG.info('Set metadata on s3://%s/%s to %s', self.bucket, obj['Key'], ' '.join(args))

    # --------------------------------------------------------------------------
    # noinspection PyUnusedLocal
    @staticmethod
    def then_exec(obj, args):
        """
        Run a command.

        This does not use a shell by default. If that is required then the
        command should be "/bin/sh -c ...".

        DO NOT USE with untrusted input (i.e. on an untrusted bucket).

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        Command and its args.

        :type obj:          dict[str, T]
        :type args:         list[str]

        :raise ActionError: If the command returns non-zero exit status.
        """

        if not args:
            raise ValueError('One or more arguments required for "exec" command')

        retcode = subprocess.call(args, stdin=subprocess.DEVNULL)
        if retcode:
            # Command failed
            raise ActionError('then_exec failed - return status {}'.format(retcode))

    # --------------------------------------------------------------------------
    def then_acl(self, obj, args):
        """
        Set the ACL on the object

        :param obj:         S3 object dict (see s3_list_bucket())
        :param args:        The new pre-canned ACL.

        :type obj:          dict[str, T]
        :type args:         str
        """

        if len(args) != 1:
            raise ValueError('Exactly one argument required for "acl" command')

        try:
            self.s3.put_object_acl(
                Bucket=self.bucket,
                Key=obj['Key'],
                ACL=args[0]
            )
        except Exception as e:
            raise ActionError('Set ACL on s3://{}/{} to {} failed - {}'.format(
                self.bucket, obj['Key'], args[0], e))

        LOG.info('Set ACL on s3://%s/%s to %s', self.bucket, obj['Key'], args[0])

    # endregion then commands
    # ..........................................................................


# ------------------------------------------------------------------------------
class ActionFile(object):
    """
    Interface to the S3 action file structure
    """

    # --------------------------------------------------------------------------
    def __init__(self, action_file, *, bucket=None, prefix=None, aws_session=None):
        """
        Read the specified action file (which may be in S3) and extract action
        specs one by one. This is a generator.

        :param action_file:     Name of the action file. If it starts with s3://
                                it will be downloaded from S3.
        :param bucket:          Name of the target bucket. A bucket key in the
                                action specification will override this. If
                                the action specification does not have a bucket
                                key then this parameter is required.
                                Default None.
        :param prefix:          Prefix in the target bucket. A prefix key in
                                the action specification will override this.
                                Default None.
        :param aws_session:     A boto3 Session object. If not supplied a
                                default session will be used.
        :type action_file:      str
        :type bucket:           str
        :type prefix:           str
        :type aws_session:      boto3.Session

        """

        self.action_file = action_file
        self.aws_session = aws_session if aws_session else boto3.Session()
        self.s3 = self.aws_session.client('s3')

        if action_file.startswith('s3://'):
            b, k = s3_split(action_file)
            if not bucket or not k:
                raise Exception('Bad action file name')
            data = s3_load_yaml(b, k, aws_session=aws_session)
            LOG.debug('Read %s from S3', action_file)
        else:
            with open(action_file, 'r') as fp:
                data = yaml.safe_load(fp)
            LOG.debug('Read %s locally', action_file)

        if not isinstance(data, dict):
            raise Exception('Missing or malformed actions list')

        action_data = data.get('actions')

        if not isinstance(action_data, list):
            raise Exception('Missing or malformed actions list')

        self.actions = []
        for a_idx, a in enumerate(action_data, start=1):
            if not isinstance(a, dict):
                raise ValueError('Action #{} malformed - must be dict not {}'.format(a_idx, type(a)))

            self.actions.append(
                Action(
                    id=a.get('id'),
                    bucket=a.get('bucket', bucket),
                    prefix=a.get('prefix', prefix),
                    if_list=a.get('if'),
                    then_list=a.get('then'),
                    aws_session=self.aws_session
                )
            )

    # --------------------------------------------------------------------------
    def do_actions(self, *, action_ids=None, dry_run=False):
        """
        Perform the specified actions.

        :param action_ids:      A list of action IDs to run. If empty or None
                                then all actions are run.
        :param dry_run:         If True, report what would be done without doing
                                it. Default False.

        :type action_ids:       list[str] | tuple[str] | set[str]
        :type dry_run:          bool
        """

        for a in self.actions:

            if action_ids and a.id not in action_ids:
                LOG.debug('Skipping action %s:%s', self.action_file, a.id)
                continue

            LOG.debug('Doing action %s:%s', self.action_file, a.id)

            try:
                a.do_actions(dry_run=dry_run)
            except Exception as e:
                raise Exception('Action {} - {}'.format(a.id, e))


# ------------------------------------------------------------------------------
def process_cli_args():
    """
    Process the command line arguments.
    :return:    The args namespace.
    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='Perform actions on S3 objects.',
        epilog='See https://bitbucket.org/murrayandrews/s3it for more information.'
    )

    argp.add_argument('-a', '--action', metavar='ACTION', dest='actions', action='append',
                      help='Run only the action with the specified ID. Can be'
                           ' repeated to select multiple actions.')

    argp.add_argument('-b', '--bucket', action='store',
                      help='Bucket name. Used (and required) if the action'
                           ' specifications do not specify a target bucket.')

    argp.add_argument('-d', '--dry-run', dest='dry_run', action='store_true',
                      help='Do a dry run. No modify actions are performed but actions'
                           ' that would be performed are identified.')

    argp.add_argument('--lock-file', dest='lock_file', action='store',
                      help='Create a lock file with the given name to prevent'
                           ' two instances using the same lock file from running'
                           ' simultaneously.')

    argp.add_argument('-p', '--prefix', action='store',
                      help='Prefix in the target bucket. Used if the action'
                           ' specifications do not specify a prefix.')

    argp.add_argument('--profile', action='store', help='As for AWS CLI.')

    argp.add_argument('-v', '--version', action='version', version=__version__,
                      help='Print version and exit.')

    argp.add_argument('action_files', nargs='+', metavar='action-file',
                      help='Action definition files. Prefix with s3:// for S3 based files.')

    # ----------------------------------------
    # Logging args

    argl = argp.add_argument_group('logging arguments')

    argl.add_argument('-c', '--no-colour', '--no-color', dest='colour', action='store_false',
                      default=True, help='Don\'t use colour in information messages.')

    argl.add_argument('-l', '--level', metavar='LEVEL', default=LOGLEVEL,
                      help='Print messages of a given severity level or above.'
                           ' The standard logging level names are available but info,'
                           ' warning and error are most useful. Default is {}.'.format(LOGLEVEL))

    argl.add_argument('--log', action='store',
                      help='Log to the specified target. This can be either a file'
                           ' name or a syslog facility with an @ prefix (e.g. @local0).')

    argl.add_argument('--tag', action='store', default=PROG,
                      help='Tag log entries with the specified value. The default is {}.'.format(PROG))

    args = argp.parse_args()

    if not args.actions:
        args.actions = []

    return args


# ------------------------------------------------------------------------------
def main():
    """
    Setup logging and process action specification files.

    """

    setup_logging(level=LOGLEVEL, name=LOGNAME, target=LOGTARGET, prefix=PROG)
    args = process_cli_args()
    setup_logging(
        level=args.level,
        name=LOGNAME,
        target=args.log,
        colour=args.colour,
        prefix=args.tag if args.tag else PROG
    )

    if args.lock_file and not lock_file(args.lock_file):
        LOG.warning('Cannot get a lock - is another instance already running?')
        return 0

    aws_session = boto3.Session(profile_name=args.profile)

    for action_file in args.action_files:

        af = ActionFile(
            action_file,
            bucket=args.bucket,
            prefix=args.prefix,
            aws_session=aws_session
        )
        try:
            af.do_actions(action_ids=args.actions, dry_run=args.dry_run)
        except Exception as e:
            raise Exception('{}: {}'.format(action_file, e))


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    # exit(main())  # Uncomment for debugging
    try:
        exit(main())
    except Exception as ex:
        LOG.error('%s', ex)
        exit(1)
    except KeyboardInterrupt:
        LOG.warning('Interrupt')
        exit(2)
